<?php

use Illuminate\Database\Seeder;
use App\User;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(!User::where('role',1)->first()) {
            User::create([
                'name' => 'Admin',
                'email' => 'a@mail.ru',
                'role' => 1,
                'password' =>bcrypt('123'),
            ]);
        }
    }
}
