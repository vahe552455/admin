@extends('layouts.app')
@section('graph')
@endsection
{{--@section('notifications')
@endsection--}}
@section('content')
    <div class="container_fluid">
    <div class=" row justify-content-center">
        <div class="col-md-6">
            <div class="card  mt-4">
                <div class ='card-header  '> edit news</div>
                <div class="card-body ">
                    <form method="POST" action="{{ route('news.update', $news->id) }}" class="container" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class=" ">
                            <div class="text-dark ">
                                <div class="form-group row">
                                    <label for="name" class="col-md-2 col-form-label ">{{ __('Name') }}</label>
                                    <div class="col-md-8">
                                        <input id="name" type="text"  value='{{$news->title}}' class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="title" required autofocus>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="description" class="col-md-2 col-form-label pr-0 ">Description</label>
                                    <div class="col-md-8">
                                        <textarea  id='description' class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="description"  required >{{$news->description}}</textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="select_categ" class="col-md-2 col-form-label ">Category</label>
                                    <div class="col-md-8 ">
                                        <select name="categories[]" id="select_categ" class="form-control" multiple="multiple">
                                            @foreach($categories as $category)

                                                <option value="{{$category->id}}" {{$category->news()->find($news->id) ? 'selected' : ''       }}>{{$category->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row mb-0">
                                    <div class="col-md-3 offset-md-9">
                                        <button type="submit" class="btn btn-primary ">
                                            Change
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                @if($errors->any())
                    <div class="card-footer">
                        <div class="alert alert-danger">{{$errors->first()}}</div>
                    </div>
                @endif
            </div>
        </div>
    </div>
        <div class="row justify-content-center mt-5">


            <form action="" enctype="multipart/form-data">
                <div class="form-group row">
                    <div class="col-md-2">  Upload</div>
                    <div class="input-group mb-3 col-md-8">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="inputGroupFile02" name="image[]" multiple="multiple">
                            <label class="custom-file-label" for="inputGroupFile02" aria-describedby="inputGroupFileAddon02">Choose file</label>
                        </div>
                    </div>
                </div>
            </form>

        </div>
        <div class="row">
            <!-- Page Content -->
            <div class="container">

                <h1 class="font-weight-light text-center text-lg-left mt-4 mb-0">News galery</h1>

                <hr class="mt-2 mb-5">

                <div class="row text-center text-lg-left">

                    <div class="col-lg-3 col-md-4 col-6">
                        <a href="#" class="d-block mb-4 h-100">
                            <img class="img-fluid img-thumbnail" src="https://source.unsplash.com/pWkk7iiCoDM/400x300" alt="">
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-4 col-6">
                        <a href="#" class="d-block mb-4 h-100">
                            <img class="img-fluid img-thumbnail" src="https://source.unsplash.com/aob0ukAYfuI/400x300" alt="">
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-4 col-6">
                        <a href="#" class="d-block mb-4 h-100">
                            <img class="img-fluid img-thumbnail" src="https://source.unsplash.com/EUfxH-pze7s/400x300" alt="">
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-4 col-6">
                        <a href="#" class="d-block mb-4 h-100">
                            <img class="img-fluid img-thumbnail" src="https://source.unsplash.com/M185_qYH8vg/400x300" alt="">
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-4 col-6">
                        <a href="#" class="d-block mb-4 h-100">
                            <img class="img-fluid img-thumbnail" src="https://source.unsplash.com/sesveuG_rNo/400x300" alt="">
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-4 col-6">
                        <a href="#" class="d-block mb-4 h-100">
                            <img class="img-fluid img-thumbnail" src="https://source.unsplash.com/AvhMzHwiE_0/400x300" alt="">
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-4 col-6">
                        <a href="#" class="d-block mb-4 h-100">
                            <img class="img-fluid img-thumbnail" src="https://source.unsplash.com/2gYsZUmockw/400x300" alt="">
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-4 col-6">
                        <a href="#" class="d-block mb-4 h-100">
                            <img class="img-fluid img-thumbnail" src="https://source.unsplash.com/EMSDtjVHdQ8/400x300" alt="">
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-4 col-6">
                        <a href="#" class="d-block mb-4 h-100">
                            <img class="img-fluid img-thumbnail" src="https://source.unsplash.com/8mUEy0ABdNE/400x300" alt="">
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-4 col-6">
                        <a href="#" class="d-block mb-4 h-100">
                            <img class="img-fluid img-thumbnail" src="https://source.unsplash.com/G9Rfc1qccH4/400x300" alt="">
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-4 col-6">
                        <a href="#" class="d-block mb-4 h-100">
                            <img class="img-fluid img-thumbnail" src="https://source.unsplash.com/aJeH0KcFkuc/400x300" alt="">
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-4 col-6">
                        <a href="#" class="d-block mb-4 h-100">
                            <img class="img-fluid img-thumbnail" src="https://source.unsplash.com/p2TQ-3Bh3Oo/400x300" alt="">
                        </a>
                    </div>
                </div>

            </div>
                <!-- /.container -->
        </div>
    </div>
@endsection