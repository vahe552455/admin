@extends('layouts.app')
@section('graph')
@endsection
@section('style')
    <style>
        .sized_img {
            max-width: 100px !important;
        }
    </style>
@endsection
@section('datatable')
    {{--<img src="{{App\Models\News::find(94)->getImgPath('images/6k0L30ZymKmPpbp81tlMR7Yi8sEX3SFairT9RGxq.png')}}" alt="">--}}

    <!-- DataTables Example -->
    <div class="card mb-3">
        <div class="card-header clearfix">
            <i class="fas fa-table"></i>
            Data Table Example
            <a href="{{route('news.create')}}" class="btn btn-info float-right">create </a>

        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Image</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Image</th>
                        <th>Action</th>
                    </tr>
                    </tfoot>
                    <tbody>

                    {{--{{dd($news->find(94)->images()->first()->getImgPath(1))}}--}}

                    <img src="{{storage_path().'/images/9litRrUes0gjrLXxGl7FtUZCv0pq4qfPeU4S3KE2.jpeg'}}" alt="">
                    @foreach($news  as $item)
                        <tr id="{{$item->id}}" class="text-center">
                            <td><a href="{{route('news.edit',['id'=>$item->id])}}">{{$item->title}}</a></td>
                            <td>{{$item->description}}</td>
                            <td><img src="{{$item->images()->first() ? $item->images->first()->getImgPath() : ''}}"
                                     alt="" class="img-fluid sized_img"></td>
                            <td class="text-center">
                                <a href="{{route('news.destroy',$item->id)}}"
                                   data-toggle="modal" data-target="#deleteModal" class='closs-item'
                                   data-id="{{$item->id}}" data-method='delete'><i
                                            class="fas fa-times"></i> </a>
                            </td>
                        </tr>
                    @endforeach
                    {{$news->links('vendor.pagination.bootstrap-4')}}


                    </tbody>

                </table>
            </div>
        </div>
        <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
    </div>
    </div>

    {{--delete modal--}}

    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         style="display: none;" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">You want to delete this item?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Delete this item?</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    {{-- <form action="{{route('admin.destroy',$user->id)}}" method="POST" class='closs-item' data -method='delete'>               @csrf
                         <button class="btn btn-primary" type="submit" >Delete</button>
                     </form>--}}
                    <a href="" class='closs-this btn btn-info' data-method='delete' data-dismis="modal">Delete</a>
                </div>
            </div>
        </div>
    </div>
@endsection
