@extends('layouts.app')
@section('graph')
@endsection
{{--@section('notifications')
@endsection--}}
@section('content')
    <div class=" row justify-content-center">
        <div class="col-md-6">
            <div class="card  mt-4">
                <div class ='card-header  '> Add news</div>
                <div class="card-body ">
                    <form method="POST" action="{{ route('news.store') }}" class="container" enctype="multipart/form-data">
                        @csrf
                        <div class=" ">
                            <div class="text-dark ">
                                <div class="form-group row">
                                    <label for="name" class="col-md-2 col-form-label ">{{ __('Name') }}</label>
                                    <div class="col-md-8">
                                        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="title" required autofocus>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="description" class="col-md-2 col-form-label pr-0 ">Description</label>
                                    <div class="col-md-8">
                                        <textarea  id='description' class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="description"  required >  </textarea>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="select_categ" class="col-md-2 col-form-label ">Category</label>
                                    <div class="col-md-8 ">
                                        <select name="categories[]" id="select_categ" class="form-control" multiple="multiple">
                                            @foreach($categories as $category)
                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                                @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-2">  Upload</div>
                                    <div class="input-group mb-3 col-md-8">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="inputGroupFile02" name="image[]" multiple="multiple">
                                            <label class="custom-file-label" for="inputGroupFile02" aria-describedby="inputGroupFileAddon02">Choose file</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row mb-0">
                                    <div class="col-md-3 offset-md-9">
                                        <button type="submit" class="btn btn-primary ">
                                            Create
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                @if($errors->any())

                <div class="card-footer">
                    <div class="alert alert-danger">{{$errors->first()}}</div>
                </div>
                    @endif
            </div>
        </div>
    </div>

@endsection