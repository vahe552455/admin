@extends('layouts.app')
@section('graph')
@endsection
@section('datatable')

    <!-- DataTables Example -->

    <div class="card mb-3 mt-5">
        <div class="card-header">
            <i class="fas fa-table"></i>
            Data Table Example</div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Country</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Country</th>
                        <th>Action</th>
                    </tr>
                    </tfoot>
                    <tbody>
                    @foreach($users as $user)
                        <tr id="{{$user->id}}">
                            <td ><a href="{{route('user.edit',['id'=>$user->id])}}" data-toggle="modal" data-target="#openModal" data-user="{{$user}}"  class="getname">{{$user->name}}</a></td>
                            <td class="getmail">{{$user->email}}</td>
                            <td class="getcountry">{{isset($user->country->name) ? $user->country->name : ''}}</td>
                            <td class="text-center">
                                <a data-toggle="modal" data-target="#deleteModal"  data-id="{{$user->id}}" href='{{route('admin.destroy',['id'=>$user->id])}}' class="closs-item"><i class="fas fa-times" ></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            {{$users->links('vendor.pagination.bootstrap-4')}}
        </div>
        <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
    </div>
    </div>

    {{--modal delete--}}
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">You want to delete this item?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Delete this item? </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    {{-- <form action="{{route('admin.destroy',$user->id)}}" method="POST" class='closs-item' data -method='delete'>               @csrf
                         <button class="btn btn-primary" type="submit" >Delete</button>
                     </form>--}}
                    <a href=""  class='closs-this btn btn-info' data-method='delete'  data-dismis="modal">Delete</a>
                </div>
            </div>
        </div>
    </div>
    {{--modal edit--}}
    <div class="modal fade" id="openModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle"> Change user information</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="POST" action="{{ route('user.update', 1) }}">
                        @csrf
                        <input type="hidden" name="current_id" class="hidden_input" >
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">Email</label>
                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="email"  required >
                                {{--  @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif--}}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="select_country" class="col-md-4 col-form-label text-md-right">Country</label>

                            <div class="col-md-6">
                                <select name="country_id" id="select_country" class="form-control" >
                                    @foreach($countries as $country)
                                        <option value="{{$country->id}}" >{{$country->name}}</option>
                                    @endforeach
                                </select>
                                {{--<input id="password-confirm" type="password" class="form-control" name="country" required>--}}
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary d-none" id="trigger_click">
                                    Add
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary"  data-click="trigger_click" data-dismiss="modal">Change</button>
                </div>
            </div>
        </div>
    </div>

    </div>
@endsection
@section('content')
    <div class="container mt-4 ">
        <div class="row align-items-center  ">
            <div class="col-6">
        {{--        <form action="" >
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroupFileAddon01">Upload user</span>
                        </div>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01">
                            <label class="custom-file-label" for="inputGroupFile01">Choose file</label>
                        </div>
                    </div>
                </form>--}}


                <form action="{{route('user.upload')}}" enctype="multipart/form-data" method="post">
                    @csrf
                    <div class="input-default-wrapper mt-5">
                        <input type="file" id="file-with-multi-file" class="input-default-js" data-multiple-target="{target} files selected" name="file">
                        {{--<input type='hidden' name="extension" value="xls">--}}
                        <label class="label-for-default-js rounded-right mb-3 alert alert-warning" for="file-with-multi-file ">
                            <span class="span-choose-file ">Choose file</span>
                            <div class="float-right span-browse">Browse</div>
                        </label>
                    </div>
                    <button class="input-group-text mt-2" type="submit" id="input2">Upload</button>
                </form>
            </div>


            <div class="col-6">
                @if($errors->any())
                <div class="alert alert-danger">{{$errors->first()}}</div>
                @endif

                <a href="{{route('user.download')}}" class="btn btn-info export">Download </a>
            </div>
            <div class="col-6">

            </div>
        </div>
    </div>
@endsection