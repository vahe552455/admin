@extends('layouts.app')
@section('graph')
@endsection
@section('datatable')
@endsection
@section('notifications')
@endsection
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <form action="{{route('user.update',['id'=>$user->id])}}"  method="POST" class="clearfix alert alert-success">
                    {{csrf_field()}}
                    @method('PUT')
                    <div class="form-group row">
                        <label for="name" class=" col-form-label col-1 text-md-right"> Name</label>
                        <div class="col-1"></div>

                        <input type="text" class="form-control col-md-6" id="name" value="{{$user->name}}" name="name">
                    </div>
                    <div class="form-group row">
                        <label for="email" class=" col-form-label col-1 text-md-right"> Email</label>
                        <div class="col-1"></div>
                        <input type="email" class="form-control col-md-6" id="email" value="{{$user->email}}" name="email">
                    </div>
                    <input type="hidden" name="current_id" value=" {{$user->id}}">
                    <button type="submit" class="btn btn-info float-right">Change</button>
                </form>
            </div>
        </div>
    </div>
    @if($errors->any())
        <div class="alert alert-danger">
            {{$errors->first()}}
        </div>
    @endif
@endsection
