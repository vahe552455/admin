@extends('layouts.app')
@section('graph')
@endsection
{{--@section('notifications')
@endsection--}}
@section('content')
    <form method="POST" action="{{ route('category.store') }} " class="container">
        @csrf
        <div class=" row justify-content-center">
            <div class="col-md-9 alert alert-success text-dark ">
                <div class="form-group row">
                    <label for="name" class="col-md-2 col-form-label text-md-right">{{ __('Name') }}</label>

                    <div class="col-md-8">
                        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name"  required autofocus>
                    </div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col-md-3 offset-md-9">
                        <button type="submit" class="btn btn-primary " id="trigger_click">
                            Add
                        </button>
                    </div>
                </div>
            </div>
        </div>

    </form>
@endsection