@extends('layouts.app')
@section('graph')
@endsection
{{--@section('notifications')
@endsection--}}
@section('content')
            <div class=" row justify-content-center">
                <div class="col-md-6">
                    <div class="card">
                        <div class ='card-header  '> Add new category</div>
                        <div class="card-body ">
                            <form method="POST" action="{{ route('category.update',$category->id) }}" class="container">
                                @csrf
                                @method('PUT')
                                <div class="  ">
                                    <div class="text-dark ">
                                        <div class="form-group row">
                                            <label for="name" class="col-md-2 col-form-label ">{{ __('Name') }}</label>
                                            <div class="col-md-8">
                                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name"  value="{{$category->name}}" required autofocus>
                                            </div>
                                        </div>
                                        {{--<div class="form-group row">
                                            <label for="select_country" class="col-md-2 col-form-label ">Country</label>

                                            <div class="col-md-8 ">
                                                <select name="country_id" id="select_country" class="form-control" >
                                                    <option value="1">sfaghfb</option>
                                                </select>
                                            </div>
                                        </div>--}}
                                        <div class="form-group row mb-0">
                                            <div class="col-md-3 offset-md-9">
                                                <button type="submit" class="btn btn-primary " id="trigger_click">
                                                    Update
                                                </button>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>


            </div>

@endsection