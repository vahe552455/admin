@extends('layouts.app')
@section('graph')
@endsection
@section('datatable')

    <!-- DataTables Example -->
    <div class="card mb-3">
        <div class="card-header clearfix">
            <i class="fas fa-table"></i>
            Data Table Example
            <a href="{{route('category.create')}}" class="btn btn-info float-right" >create </a>

        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <th>Name</th>
                        <th>Action</th>
                    </tr>
                    </tfoot>
                    <tbody>
                    @foreach($categories as $category)
                        <tr id="{{$category->id}}">
                            <td><a href="{{route('category.edit',['id'=>$category->id])}}">{{$category->name}}</a></td>
                            <td class="text-center"><a href="{{route('category.destroy',$category->id)}}" data-toggle="modal" data-target="#deleteModal"  class='closs-item' data-id="{{$category->id}}"  data-method='delete'><i class="fas fa-times"></i> </a></td>
                        </tr>
                    @endforeach
                    </tbody>

                </table>
            </div>
        </div>
        <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
    </div>
    </div>

{{--delete modal--}}

    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">You want to delete this item?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Delete this item? </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                    {{-- <form action="{{route('admin.destroy',$user->id)}}" method="POST" class='closs-item' data -method='delete'>               @csrf
                         <button class="btn btn-primary" type="submit" >Delete</button>
                     </form>--}}
                    <a href=""  class='closs-this btn btn-info' data-method='delete'  data-dismis="modal">Delete</a>
                </div>
            </div>
        </div>
    </div>
@endsection
