<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware'=>'is_admin','prefix'=>'admin','namespace'=>'Admin\Users'],function () {
  Route::get('/','UserController@index')->name('admin.index');
  Route::get('/users','UserController@users')->name('admin.users');
  Route::get('/users/create','UserController@create')->name('user.register');
  Route::post('/users/store','UserController@store')->name('user.store');
  Route::put('/users/{id}','UserController@update')->name('user.update');
  Route::get('/users/{id}','UserController@edit')->name('user.edit');
  Route::post('/users/upload','UserController@upload')->name('user.upload');
  Route::get('/users/download/1','UserController@download')->name('user.download');
  Route::delete('/delete/{id}','UserController@destroy')->name('admin.destroy');
});

Route::group(['prefix'=>'category', 'as' => 'category.','namespace'=>'Admin\Categories'],function () {
    Route::resource('','CategoryController',['parameters' => [
        '' => 'id',
    ]]);


});
Route::group(['prefix'=>'news', 'as' => 'news.','namespace'=>'Admin\News'],function () {
    Route::resource('','NewsController',['parameters' => [
        '' => 'id',
    ]]);


});