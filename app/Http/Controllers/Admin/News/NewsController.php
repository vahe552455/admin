<?php

namespace App\Http\Controllers\Admin\News;

use App\Contracts\CategoryInterface;
use App\Contracts\NewsInterface;
use App\Http\Requests\NewsCreate;
use App\Models\News;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected  $newsRepo;
    protected  $categoryRepo;

    public function __construct(NewsInterface $news,CategoryInterface $category)
    {
        $this->newsRepo = $news;
        $this->categoryRepo = $category;
    }

    public function index()
    {
       $news =  $this->newsRepo->all();

        return view('admin.news.index',compact('news'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   $categories = $this->categoryRepo->all();
        return view('admin.news.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(NewsCreate $request)
    {
      $this->newsRepo->store($request);
      return redirect()->route('news.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $news = $this->newsRepo->edit($id);

        $categories = $this->categoryRepo->all();
        return view('admin.news.edit',compact(['news','categories']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(NewsCreate $request, $id)
    {
         $this->newsRepo->update($id, $request);
         return redirect()->route('news.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->newsRepo->deleteNews($id);
        return response()->json(['id'=>$id]);
    }
}
