<?php

namespace App\Http\Controllers\Admin\Users;

use App\Contracts\CountryInterface;
use App\Contracts\UserInterface;
use App\Exports\UsersExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\ExcelFile;
use App\Http\Requests\UserChange;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * @var Repository
     */
    private $userRepo;
    private $countryRepo;
    private $excel;

    public function __construct(UserInterface $userRepo,CountryInterface $countries, Excel $excel)
    {
        // set the model
        $this->userRepo = $userRepo;
        $this->excel = $excel;
        $this->countryRepo = $countries;
    }


    public function index()
    {
//        dd( $this->model->all());
        return view('admin.index');
    }
    public function users()
    {
        $users = $this->userRepo->all();
        $countries= $this->countryRepo->all();
        return view('admin.users',compact('users','countries'));
    }


    public function upload(Request $request) {

    $x  = Validator::make(
            [
                'file'      => $request->file,
                'extension' => strtolower($request->file->getClientOriginalExtension()),
            ],
            [
                'file'          => 'required',
                'extension'      => 'required|in:doc,csv,xlsx,xls, docx,ppt,odt,ods,odp',
            ])->validate();
$users = new UsersImport();
//        $extension = ucfirst($request->file('file')->clientExtension());
      $data =  Excel::import(new UsersImport, $request->file, 's3', 'Xlsx');


        $this->userRepo->resetPass((new UsersExport())->collection());

        return redirect()->back()->withErrors(['message'=>'data in your DB now']);
    }

    public function download()
    {
            return Excel::download(new UsersExport(), 'asdasd.xls', \Maatwebsite\Excel\Excel::CSV);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(CountryInterface $country)
    {
        $countries = $country->all();
        return view('admin.create',compact('countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserChange $request)
    {
        $user = $this->userRepo->create($request->validated());

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = $this->userRepo->find($id);
        return view('admin.edit',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserChange $request, $id )
    {
        $data =  $request->all();

        $info =  $this->userRepo->update($data,$id);
        return response()->json(json_encode($info->load('country')));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->userRepo->deleteUser($id);
        return response()->json(['id'=>$id]);
    }
}
