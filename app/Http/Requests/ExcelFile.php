<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ExcelFile extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */


    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
//       $this->extension = strtolower($this->file->getClientOriginalExtension());

        return
            [
                'file'   => 'required|mimes:application/vnd.ms-excel',
//                'extension' => 'required|in:doc,csv,xlsx,xls,docx,ppt,odt,ods,odp',
            ];

    }
/*    public function getValidatorInstance()
    {
        $this->formatPhoneNumber();

        parent::getValidatorInstance();
    }

    protected function formatPhoneNumber()
    {
        $this->request->merge([
            'extension' => strtolower($this->file->getClientOriginalExtension())
        ]);
    }*/

}
