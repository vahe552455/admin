<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserChange extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        return [
            'name'  => 'required|string',
            'email' => 'email|unique:users,email,'.$request->id,
            'country_id' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'A title is required',
            'email.unique'  => 'This email already exist ',
        ];
    }


}
