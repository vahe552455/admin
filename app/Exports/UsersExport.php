<?php

namespace App\Exports;

use App\Models\Category;
use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;

class UsersExport implements FromCollection,WithStrictNullComparison,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return User::where('role','!=',1)->get();
    }
    public function headings():array
    {
        return [
            '#',
            'Name',
            'Email',
            'Created at',
            'Updated at',
            'Updated at',
            'Updated at'
        ];
    }
}
