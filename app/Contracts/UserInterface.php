<?php namespace App\Contracts;

interface UserInterface
{
    public function all();
    public function create(array $data);
    public function update(array $data, $id);
    public function find($id);
    public  function deleteUser($id) ;
    public function resetPass($users);
}