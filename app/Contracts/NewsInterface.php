<?php
namespace App\Contracts;


Interface NewsInterface {

  public function all();
  public function store($data);
  public function deleteNews($data);
  public  function edit($id);
  public  function update($id,$data );

}