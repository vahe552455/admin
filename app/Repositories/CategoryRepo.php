<?php
namespace App\Repositories;
use App\Models\Category;
use App\Contracts\CategoryInterface;

class CategoryRepo implements CategoryInterface {
    protected $model;
    public function __construct(Category $model) {
        $this->model = $model;
    }

    public function all() {
        return $this->model->all();
    }

    public function store($data) {

        return $this->model->create($data);
    }

    public  function deleteCategory($id) {
        $this->model->findorFail($id)->delete();
        /*$data = $this->model->findorFail($id)->with('news')  ;

        $data->delete();*/
        return  true;
        $data->news()->delete();
        return true;
    }

    public  function edit($id)
    {
        return   $this->model->findorFail($id);
    }
    public function update($data , $id)
    {
        $this->model->findorFail($id)->update($data);
    }



}
