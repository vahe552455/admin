<?php
namespace App\Repositories;
use App\Contracts\NewsInterface;

use App\Models\News;
use Illuminate\Support\Facades\Storage;
class NewsRepo implements NewsInterface {

    protected $model;

    public function __construct(News $model) {
        $this->model = $model;
    }

    public function all() {
        return $this->model->with('images')->paginate(10);
    }

    public function store($data) {
        $addednews =  $this->model->create($data->except(['categories','image','_token'] ));
        foreach ($data->categories as $category_id) {
           $addednews->categories()->attach($category_id);
        }
        if($data->image) {
            foreach ($data->image as $image) {
                $imgname = Storage::disk('public')->put('images',$image );
                $addednews->images()->create(['name'=>$imgname]);
            }
        }
        return true ;
    }

/*
  public function categories($data) {
        return $this->model->create($data);
    }*/

    public  function deleteNews($id) {
       $news = $this->model->findorFail($id)->load('images');
       $news->images()->delete();
       $news->delete();
        return true;
    }

    public  function edit($id) {
        return   $this->model->find($id)->load(['images','categories']);
    }

    public function update($id , $data) {
        $this->model->find($id)->categories()->sync($data['categories']);
       $updated =  $this->model->findorFail($id)->update($data->only(['title','description']));

        return true;
    }

}
