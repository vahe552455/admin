<?php
namespace App\Repositories;
use App\Models\Country;
use App\Contracts\CountryInterface;

class CountryRepo implements CountryInterface {
protected $model;
    public function __construct(Country $model) {
        $this->model = $model;
    }
    public  function ReturnJson(){
        $data = database_path('/countries/countries.json');
        return json_decode(file_get_contents($data),true);
    }
    public function all() {
        return $this->model->all();
    }

}
