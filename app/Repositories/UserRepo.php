<?php namespace App\Repositories;

use App\Jobs\PasswordReseting;
use App\Mail\UserCreated;
use Carbon\Carbon;
use Faker\Generator as Faker;
use App\User;
use App\Contracts\UserInterface;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Password;

class UserRepo implements UserInterface
{
    // model property on class instances
    protected $model;

    // Constructor to bind model to repo
    public function __construct(User $model)
    {
        $this->model = $model;
    }

    // Get all instances of model
    public function all()
    {
        return $this->model->with('country')->where('role', 0)->latest()->paginate(10);
    }


    // create a new record in the database
    public function create(array $data)
    {
        if (isset($data['password'])) {
            $data['password'] = bcrypt($data['password']);
            return $this->model->create($data);
        } else {
            $data['password'] = bcrypt($pass = str_random(6));
            $user = $this->model->create($data);
            $response = Password::broker()->sendResetLink(
                $user->only('email')
            );
            /*  Mail::to($user)->send(new UserCreated($user));
              dd(1);*/
            return $user;
        }

    }

    // update record in the database
    public function update(array $data, $id)
    {
        $user = $this->find($id);
        $user->update([
            'name'       => $data['name'],
            'country_id' => $data['country_id'],
            'email'      => $data['email'],
        ]);
        return $user;
    }

    // remove record from the database
    public function deleteUser($id)
    {
        $this->model->findorFail($id)->delete();
        return true;
    }

    // show the record with the given id
    public function find($id)
    {
        return $this->model->findOrFail($id);
    }

    // Eager load database relationships
    public function with($relations)
    {
        return $this->model->with($relations);
    }

    public function resetPass($users)
    {
        foreach ($users as $user) {
            $send = (new PasswordReseting($user->toArray()))->delay(Carbon::now()->addSeconds(5));;
            dispatch($send);
        }
    }



}