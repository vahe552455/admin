<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class News extends Model
{
    protected $fillable= ['title','description','image','categories'];
    public function categories()
    {
        return $this->belongsToMany(Category::class,'categories_news');
    }
    public function images()
    {
        return $this->hasMany(Image::class);
    }

}
