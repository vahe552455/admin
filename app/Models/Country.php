<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
     protected $fillable =['capital','name','currency','user_id'];
    public function user() {
        return $this->hasMany('App\User');
    }}
