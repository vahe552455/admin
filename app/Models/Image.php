<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $fillable= ['name'];
    public function news() {
        return $this->belongsTo(News::class);
    }
    public function getImgPath()
    {
        return 'storage/'.$this->name;
    }
}
